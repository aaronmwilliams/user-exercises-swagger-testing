import io.swagger.client.api.ExercisesApi;
import io.swagger.client.model.Exercise;
import org.springframework.web.client.HttpClientErrorException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ExercisesTest {

    private final ExercisesApi api = new ExercisesApi();

    @Test
    public void shouldCreateExercise() {
        final String exerciseDescription = "Description of Exercise";

        //Given
        Exercise exercise = new Exercise();
        exercise.setDescription(exerciseDescription);

        //When
        Exercise exerciseCreated = api.saveExercises(exercise);

        //Then
        Assert.assertTrue(exerciseCreated.getId() > 0);
        Assert.assertTrue(exerciseCreated.getDescription().equals(exerciseDescription));
    }

    @Test
    public void shouldGetExercise() {
        final String exerciseDescription = "Description of Exercise";

        //Given
        Exercise exercise = new Exercise();
        exercise.setDescription(exerciseDescription);
        Exercise exerciseCreated = api.saveExercises(exercise);

        //When
        Exercise exerciseReturned = api.findExerciseById(exerciseCreated.getId().intValue());

        //Then
        Assert.assertTrue(exerciseReturned.getId() > 0);
        Assert.assertTrue(exerciseReturned.getDescription().equals(exerciseDescription));
    }

    @Test(expectedExceptions = {HttpClientErrorException.class})
    public void shouldDeleteExercise() {
        //Given
        Exercise exerciseToCreate = new Exercise();
        exerciseToCreate.setDescription("New Exercise Description");
        Exercise exerciseCreated = api.saveExercises(exerciseToCreate);

        //When
        api.deleteExercise(exerciseCreated.getId().intValue());

        //Then
        Assert.assertNull(api.findExerciseById(exerciseCreated.getId().intValue()));
    }

}
