import io.swagger.client.api.ExercisesApi;
import io.swagger.client.api.LogApi;
import io.swagger.client.api.UsersApi;
import io.swagger.client.model.Exercise;
import io.swagger.client.model.Log;
import io.swagger.client.model.PostLog;
import io.swagger.client.model.User;
import org.springframework.web.client.HttpClientErrorException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogTest {

    private final UsersApi usersApi = new UsersApi();
    private final ExercisesApi exercisesApi = new ExercisesApi();
    private final LogApi logApi = new LogApi();

    @Test
    public void shouldCreateLog() {
        //Given
        User user = new User();
        user.setName("User Name");
        user.setPostcode("AS345TG");
        User userCreated = usersApi.saveUser(user);

        Exercise exercise = new Exercise();
        exercise.setDescription("Paired Boxing");
        Exercise exerciseCreated = exercisesApi.saveExercises(exercise);

        PostLog postLog = new PostLog();
        postLog.setUserId(userCreated.getId());
        postLog.setExerciseId(exerciseCreated.getId().intValue());
        postLog.setDate("2010-01-01");

        //When
        Log logCreated = logApi.saveLog(postLog);

        //Then
        Assert.assertNotNull(logCreated.getId());
        Assert.assertEquals(logCreated.getUserId(), userCreated.getId());
        Assert.assertEquals(logCreated.getUserName(), user.getName());
        Assert.assertEquals(logCreated.getExerciseId(), logCreated.getExerciseId());
        Assert.assertEquals(logCreated.getDescription(), logCreated.getDescription());
        Assert.assertEquals(logCreated.getDate(), logCreated.getDate());
    }

    @Test
    public void shouldGetLog() {
        //Given
        User user = new User();
        user.setName("Testing User Name");
        user.setPostcode("AS345TG");
        User userCreated = usersApi.saveUser(user);

        Exercise exercise = new Exercise();
        exercise.setDescription("Paired Boxing");
        Exercise exerciseCreated = exercisesApi.saveExercises(exercise);

        PostLog postLog = new PostLog();
        postLog.setUserId(userCreated.getId());
        postLog.setExerciseId(exerciseCreated.getId().intValue());
        postLog.setDate("2010-01-01");

        Log logCreated = logApi.saveLog(postLog);

        //When
        Log logFetched = logApi.findLogById(logCreated.getId().intValue());

        //Then
        // TODO assert entire objects somewhere???
        Assert.assertNotNull(logCreated.getId());
        Assert.assertEquals(logFetched.getUserId(), userCreated.getId());
        Assert.assertEquals(logFetched.getUserName(), user.getName());
        Assert.assertEquals(logFetched.getExerciseId(), logCreated.getExerciseId());
        Assert.assertEquals(logFetched.getDescription(), logCreated.getDescription());
        Assert.assertEquals(logFetched.getDate(), logCreated.getDate());
    }

    @Test(expectedExceptions = {HttpClientErrorException.class})
    public void shouldDeleteLog() {
        //Given
        User user = new User();
        user.setName("User Name");
        user.setPostcode("AS345TG");
        User userCreated = usersApi.saveUser(user);

        Exercise exercise = new Exercise();
        exercise.setDescription("Paired Boxing");
        Exercise exerciseCreated = exercisesApi.saveExercises(exercise);

        PostLog postLog = new PostLog();
        postLog.setUserId(userCreated.getId());
        postLog.setExerciseId(exerciseCreated.getId().intValue());
        postLog.setDate("2010-01-01");

        Log logCreated = logApi.saveLog(postLog);

        //When
        logApi.deleteLog(logCreated.getId().intValue());

        //Then
        Assert.assertNull(logApi.findLogById(logCreated.getId().intValue()));
    }


}
