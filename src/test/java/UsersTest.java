import io.swagger.client.api.UsersApi;
import io.swagger.client.model.User;
import org.springframework.web.client.HttpClientErrorException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UsersTest {

    private final UsersApi api = new UsersApi();

    @Test
    public void shouldCreateUser() {
        final String name = "Test Name";
        final String postcode = "AE123FD";

        //Given
        User user = new User();
        user.setName(name);
        user.setPostcode(postcode);

        //When
        User userReturned = api.saveUser(user);

        //Then
        Assert.assertTrue(userReturned.getName().equals(name));
        Assert.assertTrue(userReturned.getPostcode().equals(postcode));
    }

    @Test
    public void shouldGetUser() {
        //Given
        User userToCreate = new User();
        userToCreate.setName("User Name");
        userToCreate.setPostcode("AS345TG");
        User userCreated = api.saveUser(userToCreate);

        //When
        User userReturned = api.findUserById(userCreated.getId().intValue());

        //Then
        Assert.assertTrue(userCreated.getName().equals("User Name"));
    }

    @Test(expectedExceptions = {HttpClientErrorException.class})
    public void shouldDeleteUser() {
        //Given
        User userToCreate = new User();
        userToCreate.setName("New User");
        userToCreate.setPostcode("AA111AA");
        User userCreated = api.saveUser(userToCreate);

        //When
        api.deleteUser(userCreated.getId().intValue());

        //Then
        Assert.assertNull(api.findUserById(userCreated.getId().intValue()));
    }

}
