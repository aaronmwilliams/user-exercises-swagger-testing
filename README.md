# user-exercises-swagger-testing

### Purpose of project
This testing framework pulls in a *swagger-java-client* library generated from the swagger file in *user-exercises-rest* and runs some simple TestNG API tests against the service.

### Installing and Running

You will need to have service *user-exercises-rest* running.

This project uses `mavenLocal()`

You will need to also checkout *swagger-java-client*. Build the project with `./gradlw clean build` and then `gradlew install`.

If you have some trouble with TestNG library you can consider manually downloading the TestNG .jar and POM file in your .m2 directory.

Once the project build and compiles run the tests with the `./gradlew cleanTest test` command.

### Supporting Repositories

The following testing frameworks can be used to test this service when running.

user-exercises-rest: https://bitbucket.org/aaronmwilliams/user-exercises-rest

swagger-java-client: https://bitbucket.org/aaronmwilliams/swagger-java-client

